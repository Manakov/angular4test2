import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.css']
})
export class ActionsComponent implements OnInit {
  @Output() itemSelected = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  selectItem(itemLevel: string) {
    this.itemSelected.emit(itemLevel);
  }
}
