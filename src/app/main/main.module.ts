import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoComponent } from './info/info.component';
import { ActionsComponent } from './actions/actions.component';

@NgModule({
  imports: [
    CommonModule
  ]
})
export class MainModule { }
