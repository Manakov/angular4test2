import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  developers: { name: string, age: number, job: string }[] = [
    {
      name: 'Dmitry Kashuba',
      age: 30,
      job: 'Lead JavaScript Developer'
    },
    {
      name: 'John Dou',
      age: 25,
      job: 'Senior Front-End Developer'
    }
  ];
  currentDeveloper: { name: string, age: number, job: string } = this.developers[0];

  itemSelected(itemLevel: string) {
    let currentDeveloperIndex = 0;

    switch (itemLevel) {
      case 'lead':
        currentDeveloperIndex = 0;
        break;

      case 'junior':
        currentDeveloperIndex = 1;
        break;
    }

    this.currentDeveloper = this.developers[currentDeveloperIndex];
  }
}
